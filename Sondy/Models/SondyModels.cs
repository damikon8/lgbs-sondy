﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;

namespace Sondy.Models
{
    public class Sonda
    {
        public string pytanie;
        public string data;
        public int liczbaOdpowiedzi;
        public List<string> odpowiedzi;
        public List<int> wyniki;

        public Sonda()
        {
            pytanie = "";
            data = "";
            liczbaOdpowiedzi = 0;
            odpowiedzi = new List<string>();
            wyniki = new List<int>();
        }
        public int getAllNoumberOfAnswers()
        {
            int suma = 0;
            foreach (var item in wyniki)
            {
                suma += item;
            }
            return suma;
        }
        public int percentOfTotal(int ilosc)
        {
            if(getAllNoumberOfAnswers()>0) return (int)(ilosc * 100 / getAllNoumberOfAnswers());
            return 0;
        }
    }

    public class Sondy
    {
        public List<Sonda> ListaSond;
        public Sonda AktualnaSonda;

        private int idAktualnejSondy;
        private string aktualnaData;  //    5.3.2012    

    public Sondy()
        {
            ListaSond = new List<Sonda>();
            aktualnaData = DateTime.Today.ToString("d"); //    5.3.2012
            parseFromFile();
            idAktualnejSondy = -1;
            AktualnaSonda = null;
            int i = 0;
            foreach (var SingleSonda in ListaSond)
            {                
                if(SingleSonda.data == aktualnaData) { idAktualnejSondy = i; break; }
                i++;
            }
            if (idAktualnejSondy >= 0) AktualnaSonda = ListaSond[idAktualnejSondy];

        }
        public void parseFromFile()
        {
                List<Sonda> listaSond = new List<Sonda>();
                string path = HttpContext.Current.Server.MapPath("~/Models/SondyXML.xml");
                //string path = @"c:\users\dkowalczyk\documents\visual studio 2015\Projects\Sondy\Sondy\Models\SondyXML.xml";
                string xmlString = File.ReadAllText(path);

                // Create an XmlReader
                using (XmlReader reader = XmlReader.Create(new StringReader(xmlString)))
                {
                    while (reader.ReadToFollowing("Sonda"))
                    {
                        Sonda singleSonda = new Sonda();
                        reader.ReadToFollowing("Pytanie");
                        singleSonda.pytanie = reader.ReadElementContentAsString();
                        reader.ReadToFollowing("Date");
                        singleSonda.data = reader.ReadElementContentAsString();
                        reader.ReadToFollowing("AnswerNo");
                        singleSonda.liczbaOdpowiedzi = reader.ReadElementContentAsInt();
                        for (int i = 0; i < singleSonda.liczbaOdpowiedzi; i++)
                        {
                            reader.ReadToFollowing("Answer");
                            singleSonda.odpowiedzi.Add(reader.ReadElementContentAsString());
                        }
                        reader.ReadToFollowing("Results");
                        for (int i = 0; i < singleSonda.liczbaOdpowiedzi; i++)
                        {
                            reader.ReadToFollowing("Result");
                            singleSonda.wyniki.Add(reader.ReadElementContentAsInt());
                        }
                        listaSond.Add(singleSonda);
                    }
                }
                ListaSond = listaSond;
        }
        public void parseToFile()
        {
                string output = "<Sondy>";
                for (int i = 0; i < ListaSond.Count; i++)
                {
                    output += "<Sonda>\n";
                    output += " <Pytanie>" + ListaSond[i].pytanie + "</Pytanie>\n";
                    output += " <Date>" + ListaSond[i].data + "</Date>\n";
                    output += " <AnswerNo>" + ListaSond[i].liczbaOdpowiedzi.ToString() + "</AnswerNo>\n";
                    output += "<Answers>\n";
                    for (int j = 0; j < ListaSond[i].liczbaOdpowiedzi; j++)
                    {
                        output += " <Answer>" + ListaSond[i].odpowiedzi[j] + "</Answer>\n";
                    }
                    output += "</Answers>\n";
                    output += "<Results>\n";
                    for (int j = 0; j < ListaSond[i].liczbaOdpowiedzi; j++)
                    {
                        output += " <Result>" + ListaSond[i].wyniki[j].ToString() + "</Result>\n";
                    }
                    output += "</Results>\n";
                    output += "</Sonda>\n";
                }
                output += "</Sondy>\n";
                System.IO.StreamWriter file = new System.IO.StreamWriter(HttpContext.Current.Server.MapPath("~/Models/SondyXML.xml"));
                file.WriteLine(output);
                file.Dispose();
        }

    }
}

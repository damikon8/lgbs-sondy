﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sondy.Controllers
{
    public class SondyController : Controller
    {
        Sondy.Models.Sondy Obiekt = new Models.Sondy();
        HttpCookie SondaCookie;
        // GET: Sondy
        public ActionResult Index()
        {
            return View(Obiekt);
        }
        public ActionResult Summary()
        {
            return View(Obiekt);
        }

        [HttpGet]
        public ActionResult Post()
        {
            if (Request.Cookies["SondaCookie"] != null)
            {
                return View("Sorry");
            }
                return View(Obiekt);
        }

        public ActionResult Ajax()
        {
            if (Request.Cookies["SondaCookie"] != null)
            {
                return View("Sorry");
            }
            return View(Obiekt);
        }


        //[HttpPost]
        public ActionResult Answer()
        {
            SondaCookie = new HttpCookie("SondaCookie");
            SondaCookie.Expires = DateTime.Today.AddDays(1).AddHours(1);
            Response.Cookies.Add(SondaCookie);

            if (Request.Form["Radios"] == null) return View(Obiekt.AktualnaSonda);
            Obiekt.AktualnaSonda.wyniki[Int32.Parse(Request.Form["Radios"])]++;
            Obiekt.parseToFile();            
            return Redirect("Answer");
        }



        public string AjaxHandler(string radios)
        {
            SondaCookie = new HttpCookie("SondaCookie");
            SondaCookie.Expires = DateTime.Today.AddDays(1).AddHours(1);
            Response.Cookies.Add(SondaCookie);
            Obiekt.AktualnaSonda.wyniki[Int32.Parse(radios)]++;
            Obiekt.parseToFile();
            string output = "";
            int sumaWynikow = Obiekt.AktualnaSonda.getAllNoumberOfAnswers();
            for (int i = 0; i < Obiekt.AktualnaSonda.liczbaOdpowiedzi; i++)
            {
                int wynik = Obiekt.AktualnaSonda.wyniki[i];
                output += "<span>"+Obiekt.AktualnaSonda.odpowiedzi[i].ToString()+" ("+wynik.ToString()+" = "+ Obiekt.AktualnaSonda.percentOfTotal(wynik).ToString() + ")</span>";
                output += "<div class='progress'>";
                output += @"<div class='progress-bar' id='Answer_"+i.ToString()+"' role='progressbar' aria-valuenow='"+ Obiekt.AktualnaSonda.percentOfTotal(wynik).ToString()+"' aria-valuemin='0' aria-valuemax='100' style='width: "+ Obiekt.AktualnaSonda.percentOfTotal(wynik).ToString() + "%;'></div>";
                output += "</div></div>";
            }
            return output;
        }

    }
}